package com.dar;

import android.app.Application;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import java.util.Arrays;
import java.util.List;
import com.reactnativenavigation.NavigationApplication;
import com.reactnativenavigation.controllers.ActivityCallbacks;

import com.facebook.react.ReactPackage;
import com.facebook.react.shell.MainReactPackage;
import com.lwansbrough.RCTCamera.RCTCameraPackage;
import com.reactnative.ivpusic.imagepicker.PickerPackage;
import com.oblador.vectoricons.VectorIconsPackage;
import com.brentvatne.react.ReactVideoPackage;
import br.com.vizir.rn.paypal.PayPalPackage;

public class MainApplication extends NavigationApplication {
	public static Activity mActivity;
    PayPalPackage mPayPalPackage;
	
	
	@Override
	public void onCreate() {
		super.onCreate();
		
		mPayPalPackage = new PayPalPackage(mActivity, 9);

		setActivityCallbacks(new ActivityCallbacks() {
			@Override
			public void onActivityCreated(Activity activity, Bundle savedInstanceState) {
				Log.i("hi","created");
				mActivity = activity;
			}

			@Override
			public void onActivityStarted(Activity activity) {
				Log.i("hi","started");

			}

			@Override
			public void onActivityResumed(Activity activity) {
				Log.i("hi","resumed");
				Log.i("hi",activity.getPackageName());
				mPayPalPackage.setContext(activity);
			}

			@Override
			public void onActivityPaused(Activity activity) {
				Log.i("hi","paused");
			}

			@Override
			public void onActivityStopped(Activity activity) {
				Log.i("hi","stoped");

			}

			@Override
			public void onActivityResult(int requestCode, int resultCode, Intent data) {
				super.onActivityResult(requestCode, resultCode, data);
				if (requestCode == 9) {
					mPayPalPackage.handleActivityResult(requestCode, resultCode, data); // <--
				} else {
					//otherModulesHandlers(requestCode, resultCode, data);
				}
			}

			@Override
			public void onActivityDestroyed(Activity activity) {

			}
		});
	}

	@Override
	public boolean isDebug() {
		return BuildConfig.DEBUG;
	}

	protected List<ReactPackage> getPackages() {
		return Arrays.<ReactPackage>asList(
			new MainReactPackage(),
			new RCTCameraPackage(),
			new PickerPackage(),
			new VectorIconsPackage(),
			new ReactVideoPackage(),
			mPayPalPackage
		);
	}

	@Override
	public List<ReactPackage> createAdditionalReactPackages() {
		return getPackages();
	}
}
