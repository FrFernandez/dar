import React, { Component } from 'react';
import {
	Text,
	View,
	StyleSheet,
	ListView,
	Image,
	TouchableHighlight, 
	TouchableOpacity
} from 'react-native';
import { Container, Content, List, ListItem, Thumbnail, Body, Spinner, Left, Right, Badge } from 'native-base';
import Fech from '../../components/Fech';
// import Auth from '../components/AuthSpinner 
const API_URL = 'https://api.somosdar.mx/challenges/search';


import store from 'react-native-simple-store';


var InfoToken = {
	status: 1
}

const navigatorStyle = {
    statusBarColor: 'black',
    statusBarTextColorScheme: 'light',
    navigationBarColor: 'black',
    navBarBackgroundColor: '#e52427',
    navBarTextColor: 'white',
    navBarButtonColor: 'white',
    tabBarButtonColor: 'red',
    tabBarSelectedButtonColor: 'red',
    tabBarBackgroundColor: 'white',
    topBarElevationShadowEnabled: false,
    navBarHideOnScroll: true,
    tabBarHidden: true,
    drawUnderTabBar: true
};

class Main extends Component {
	constructor(props) {
	  	super(props);

	  	this.props.navigator.setButtons({
  			leftButtons: [
  				{
                	id: 'sideMenu'
            	}
  			],
  			rightButtons: [],
  			animated: true
		});

	  	this.props.navigator.toggleNavBar({
            to: 'shown', 
            animated: true,
            navigatorStyle
        });

		this.props.navigator.setDrawerEnabled({
            side: 'left', // the side of the drawer since you can have two, 'left' / 'right'
            enabled: true // should the drawer be enabled or disabled (locked closed)
        });
	
	  	this.state = {
	  		dataSource: new ListView.DataSource({
	  			rowHasChanged: (row1, row2) => row1 !== row2 
	  		}),
	  		loaded: false,
	  	};
	
		
	
	}
	componentDidMount() {
		this.fetchData();
	}
	fetchData() {
		
		store.get('token')
		.then((token) => {
			InfoToken.token = token.token;
			InfoToken.tokenSecret = token.tokenSecret;
			
			fetch(API_URL,InfoToken)
			.then((response) => response.json())
			.then((response) => {
				this.setState({
					dataSource: this.state.dataSource.cloneWithRows(response.items),
					loaded: true
				})
			})
		}).catch((err) => {
			this.props.navigator.resetTo({
				screen: 'dar.login',
				animated: true,
				animationType: 'fade'
			});
		});
		
	}
	renderLoadingView() {
		return (
			<View style={styles.container}>
				<Spinner color='red' />
			</View>
		);
	}
	_pushView = (item) => {
		this.props.navigator.push({
		  screen: 'mx.somosdar.Camera',
		  title: 'Capturar',
		  passProps: {challenge: item},
		  animated: true,
		  animationType: 'fade'
		})
	}
	
	renderItems(item) {
		
		return (
			<TouchableOpacity style={{backgroundColor: '#17afbd'}} >
				<ListItem button onPress={() => this._pushView(item)}  style={{
						borderBottomColor: '#FFFFFF'
					}}>
              		<Thumbnail square source={{uri: 'https://uploads.somosdar.mx/scaled/200/'+item.photo}} />
              		<Body style={{marginLeft:18}}>
                		<Text style={styles.title}>{item.name}</Text>
                		<Text note style={styles.descript}>{item.details}</Text>
              		</Body>
              		<Right>
            			<Fech date={item.points}/>
              		</Right>
            	</ListItem>		
			</TouchableOpacity>
		);
	}
    render() {
        if (!this.state.loaded) {
        	return this.renderLoadingView();
        }
        return (
        	<ListView
        		dataSource={this.state.dataSource}
        		renderRow={this.renderItems.bind(this)}
        	/>
    	);
    }
}


const styles = StyleSheet.create({
	container: {
		flexDirection: 'row',
		justifyContent: 'center',
		alignItems: 'center',
		backgroundColor: '#F5FCFF',
	},
	title: {
		fontSize: 18,
		marginBottom: 8,
		color: '#FFFFFF',
		backgroundColor: 'rgba(52,52,52,0)',
	},
	backgroundImage: {
		justifyContent: 'center',
		alignItems: 'center',
		alignSelf: 'stretch',
		height: 150
	},
	rightContainer: {
		backgroundColor: 'rgba(52,52,52,0.5)',
		alignSelf: 'stretch',
		paddingTop: 30,
		height: 150
	},
	ListItem: {
		borderBottomColor: '#FFFFFF'
	},
	descript: {
		color: '#FFFFFF'
	}


})

export default Main;