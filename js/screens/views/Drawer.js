import React from 'react';
import { Navigation } from 'react-native-navigation';
import {
    StyleSheet, 
    View, 
    Text,
    ScrollView,
    TouchableHighlight
} from 'react-native';

import Icon from 'react-native-vector-icons/FontAwesome';

import Auth from '../../components/Auth';

const DataUser = {};

/*Auth._getData().then((success) => {
	if (!success) {
		this.props.navigator.resetTo({
			screen: 'dar.index',
			title: 'Welcome',
		});
	}else {
		DataUser = success; 
		console.log(DataUser)
	}
})*/

class Drawer extends React.Component {
    _pushMenu(number) {
        switch (number) {
            case 0:
                this.props.navigator.push({
                    screen: 'dar.main',
                    title: 'Welcome',
                });
                this.props.navigator.toggleDrawer({
                    side: 'left',
                    animated: true,
                    to: 'closed'
                });
                break;
            case 1:
                this.props.navigator.push({
                    screen: 'dar.foundations',
                    title: 'Fundaciones',
                });
                this.props.navigator.toggleDrawer({
                    side: 'left',
                    animated: true,
                    to: 'closed'
                });
                break;
            case 2:
                this.props.navigator.push({
                    screen: 'dar.points',
                    title: 'Mis Puntos',
                });
                this.props.navigator.toggleDrawer({
                    side: 'left',
                    animated: true,
                    to: 'closed'
                });
                break;
            case 3:
                this.props.navigator.push({
                    screen: 'dar.donations',
                    title: 'Mis Donaciones',
                });
                this.props.navigator.toggleDrawer({
                    side: 'left',
                    animated: true,
                    to: 'closed'
                });
                break;
            case 4:
                this.props.navigator.push({
                    screen: 'dar.winner',
                    title: 'Ganadores',
                });
                this.props.navigator.toggleDrawer({
                    side: 'left',
                    animated: true,
                    to: 'closed'
                });
                break;
            case 5:
                this.props.navigator.push({
                    screen: 'dar.instructions',
                    title: '¿Como Funciona?',
                });
                this.props.navigator.toggleDrawer({
                    side: 'left',
                    animated: true,
                    to: 'closed'
                });
                break;
            case 6:
                this.props.navigator.push({
                    screen: 'dar.account',
                    title: 'Mi perfil',
                });
                this.props.navigator.toggleDrawer({
                    side: 'left',
                    animated: true,
                    to: 'closed'
                });
                break;
            case 7:
                this.props.navigator.push({
                    screen: 'dar.privacynotice',
                    title: 'Avisos de Privacidad',
                });
                this.props.navigator.toggleDrawer({
                    side: 'left',
                    animated: true,
                    to: 'closed'
                });
                break;
            default:
                console.log('Pages Not Found');
                break;
        }
    }

	get getName(){
		return "hola";
	}
	
    render() {
        return (
            <View style={styles.container}>
                <View style={styles.DrawerTitle}>
                    <Text>
						{this.getName}
                    </Text>
                </View>
                <ScrollView>
                    <View>
                        <TouchableHighlight onPress={() => { this._pushMenu(0) }}>
                            <Text style={styles.DrawerItems}><Icon name='facebook'/> Retos de la Semana</Text>
                            
                        </TouchableHighlight>
                        <TouchableHighlight onPress={() => { this._pushMenu(1) }}>
                            <Text style={styles.DrawerItems}>Fundaciones</Text>
                        </TouchableHighlight>
                        
                    </View>
                </ScrollView>
            </View>
        );
    }
}

/*<TouchableHighlight onPress={() => { this._pushMenu(2) }}>
                            <Text style={styles.DrawerItems}>Mis Puntos</Text>
                        </TouchableHighlight>
                        <TouchableHighlight onPress={() => { this._pushMenu(3) }}>
                            <Text style={styles.DrawerItems}>Mis Donaciones</Text>
                        </TouchableHighlight>
                        <TouchableHighlight onPress={() => { this._pushMenu(4) }}>
                            <Text style={styles.DrawerItems}>Ganadores</Text>
                        </TouchableHighlight>
                        <TouchableHighlight onPress={() => { this._pushMenu(5) }}>
                            <Text style={styles.DrawerItems}>¿Como Funciona?</Text>
                        </TouchableHighlight>
                        <TouchableHighlight onPress={() => { this._pushMenu(6) }}>
                            <Text style={styles.DrawerItems}>Mi Perfil</Text>
                        </TouchableHighlight>
                        <TouchableHighlight onPress={() => { this._pushMenu(7) }}>
                            <Text style={styles.DrawerItems}>Aviso de Privacidad</Text>
                        </TouchableHighlight>*/

const styles = StyleSheet.create({
    container: {
        flex: 1,
        width: 220,
        backgroundColor: '#ffffff',
    },
    DrawerTitle: {
        padding: 20,
        paddingTop: 26,
        borderBottomColor: '#eee',
        borderBottomWidth: 2,
    },
    DrawerItems: {
        padding: 20,
    }
});

export default Drawer;