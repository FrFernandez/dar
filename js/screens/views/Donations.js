import React, { Component } from 'react';
import {
	Text, 
	View,
	StyleSheet,
	ListView,
	Image,
	TouchableHighlight,
	Button, 
} from 'react-native';
import { Spinner } from 'native-base';
// import Auth from '../components/Auth';

const API_URL = 'https://api.somosdar.mx/donations/search';

var InfoToken = {
	token      : 'XkY97z5bM',
	tokensecret: 'b779c79c-bee8-4ba0-ac64-162144bf3745',
	status: 1
}

const navigatorStyle = {
    statusBarColor: 'black',
    statusBarTextColorScheme: 'light',
    navigationBarColor: 'black',
    navBarBackgroundColor: '#e52427',
    navBarTextColor: 'white',
    navBarButtonColor: 'white',
    tabBarButtonColor: 'red',
    tabBarSelectedButtonColor: 'red',
    tabBarBackgroundColor: 'white',
    topBarElevationShadowEnabled: false,
    navBarHideOnScroll: true,
    tabBarHidden: true,
    drawUnderTabBar: true
};

class Foundations extends Component {
	constructor(props) {
	  	super(props);

	 //  	this.props.navigator.toggleNavBar({
  //           to: 'show', 
  //           animated: true,
  //           navigatorStyle
  //       });
	 //  	this.props.navigator.setDrawerEnabled({
  //           side: 'right', 
  //           enabled: true 
  //       });
  //       this.props.navigator.setButtons({
  // 			rightButtons: [
  //           	{
  //               	id: 'sideMenu'
  //           	}
  // 			], 
  // 			animated: true
		// });
	
	  	this.state = {
	  		dataSource: new ListView.DataSource({
	  			rowHasChanged: (row1, row2) => row1 !== row2 
	  		}),
	  		loaded: false,
	  	};
	}
	componentDidMount() {
		this.fetchData();
	}
	fetchData() {
		fetch(API_URL,InfoToken)
		.then((response) => response.json())
		.then((response) => {
			// console.log(response)
			this.setState({
				dataSource: this.state.dataSource.cloneWithRows(response.items),
				loaded: true

			})
		})
	}
	renderLoadingView() {
		return (
			<View style={styles.container}>
				<Spinner color='red' />
			</View>
		);
	}
	renderItems(item) {
		console.log(item)
		return (
			<TouchableHighlight>
				<Image source={{uri: 'https://uploads.somosdar.mx/'+item.photo}} style={styles.backgroundImage}>
					<View style={styles.rightContainer}>
						<Text style={styles.title}> {item.name}</Text>
						<Text style={styles.descript}>{item.details}</Text>
						<Text style={styles.cause}>{item.cause}</Text>
						<View style={styles.btn_donations}>
							<Button style={styles.btn} title='Quiero Donar'/>
						</View>
					</View>
				</Image>
			</TouchableHighlight>
		);
	}
    render() {
        if (!this.state.loaded) {
        	return this.renderLoadingView();
        }
        return (
        	<ListView
        		dataSource={this.state.dataSource}
        		renderRow={this.renderItems.bind(this)}
        		style={styles.ListView}
        	/>
    	);
    }
}

const styles = StyleSheet.create({
	container: {
		flexDirection: 'row',
		justifyContent: 'center',
		alignItems: 'center',
		backgroundColor: '#F5FCFF',
	},
	title: {
		fontSize: 27,
		marginBottom: 8,
		textAlign: 'left',
		color: '#FFFFFF',
		backgroundColor: 'rgba(52,52,52,0)',
	},
	descript: {
		fontSize: 15,
		marginTop: 5,
		textAlign: 'justify',
		color: '#FFFFFF',
		backgroundColor: 'rgba(52,52,52,0)'	
	},
	cause: {
		fontSize: 24,
		marginTop: 5,
		textAlign: 'left',
		color: '#FFFFFF',
		backgroundColor: 'rgba(52,52,52,0)'
	},
	backgroundImage: {
		justifyContent: 'center',
		alignItems: 'center',
		alignSelf: 'stretch',
		height: 150
	},
	rightContainer: {
		position: 'relative',
		backgroundColor: 'rgba(15, 108, 117, 0.55)',
		alignSelf: 'stretch',
		paddingTop: 30,
		height: 150
	},
	ListView: {
		marginBottom: 49
	},
	btn_donations: {
		position: 'absolute',
		bottom: 15,
		right: 15,
	},
	btn: {
		backgroundColor: 'rgba(0,0,0,0)',
		color: '#FFFFFF',
		borderColor: '#FFFFFF',
		borderWidth: 2,
	}


})

export default Foundations;