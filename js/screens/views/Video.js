import React, { Component } from 'react';
import { Navigation } from 'react-native-navigation';
import {
  AppRegistry,
  Dimensions,
  StyleSheet,
  Text,
  TouchableHighlight,
  TouchableOpacity,
  View
} from 'react-native';


import { Root, Toast, Container, Content, Form, Item, Input, Label, Button, Icon } from 'native-base';

import VideoPlayer from 'react-native-video-controls';

import store from 'react-native-simple-store';


const API_URL = 'https://api.somosdar.mx/submissions';

class MyClass extends Component {
	constructor(props) {
		super(props);
		this.camera = null;

		
		this.props.navigator.toggleNavBar({
		  to: 'hidden',
		  animated: false
		});
		
		
		this.props.navigator.setDrawerEnabled({
			side: 'left',
			enabled: false
		});
		
		
		this.state = {
			video: {
				source: props.path,
				paused: true
			},
			form: {
				body: '',
				challenge: props.challenge
			}
		};
	}

	sendSummision = () => {
		
		store.get('token')
		.then((token) => {
			if (token && token.token && token.tokenSecret && this.state.form.body) {
			
				Navigation.showModal({
					overrideBackPress: true,
					screen: "mx.somosdar.Loader",
					animationType: 'slide-up',
					title: "Enviando",
					passProps: {
						title: "Enviando"
					}, 
					navigatorStyle: {
						navBarHidden: true
					},
					style: {
						backgroundBlur: "dark",
						backgroundColor: "#00000080",
						tapBackgroundToDismiss: false
					}
				});
				
				
				let formData = new FormData();
				formData.append('body', this.state.form.body);  
				formData.append('challenge', this.state.form.challenge);  
				formData.append('file', { 
					name: "app_video.mp4",
					type: "video/mp4", 
					uri: this.state.video.source
				});  
				fetch(API_URL+"?token="+token.token+"&tokensecret="+token.tokenSecret, {
					method: 'POST',
					headers: {
						'Accept': 'application/json',
						'Content-Type': 'multipart/form-data',
					},
					body: formData
				})
				.then((response) => response.json())
				.then((response) => {
					
					Navigation.dismissModal();
					
					if (response.e == 0) {
						
						Toast.show({
							text: "Reto Enviado!",
							position: 'bottom',
							buttonText: 'OK'
						});
						
						this.props.navigator.resetTo({
							screen: 'dar.main',
							title: 'Retos de la Semana', 
							passProps: {},
							animated: true, 
							animationType: 'fade', 
							navigatorStyle: {}, 
							navigatorButtons: {} 
						});
					}else {
						Toast.show({
							text: response.mes,
							position: 'bottom',
							buttonText: 'OK'
						});
					}
				}) 
				.catch((err) => {
					Navigation.dismissModal();
					Toast.show({
						text: "Error de Conexión",
						position: 'bottom',
						buttonText: 'OK'
					})
				})
				
			}else
				Toast.show({
					text: 'Debes llenar el campo',
					position: 'bottom',
					buttonText: 'OK'
				});
		}).catch((err) => {
			alert(err);
			this.props.navigator.resetTo({
				screen: 'dar.login',
				animated: true,
				animationType: 'fade'
			});
		});
	}
	
	render() {
		return (
			<Root>
				<View style={styles.container}>
					<VideoPlayer
						style={styles.preview}
						source={{ uri: this.state.video.source }}
						title={ 'Video a Subir' } 
						repeat={ true }
						rate={ 1 }
						paused={true}
						navigator={ this.props.navigator }
					/>
					<View style={styles.mform}>
						<Container>
							<Content>					
								<Form>
									<Item 
									style={{
										backgroundColor: 'rgba(255,255,255,.5)'
									}}
									stackedLabel>
										<Label>Comentario</Label>
										<Input 
											style={{height:100}}
											multiline={true}
											onChangeText={(text) => this.setState({
												form: {
													...this.state.form,
													body: text,
												}
											})}
											value={this.state.form.body}
										/>
									</Item>
									<View>
										<Button style={{marginTop:25}} onPress={this.sendSummision} iconLeft light block>
											<Text style={{color:'#E52427'}}>Enviar</Text>
										</Button>
									</View>
								</Form>
							</Content>
						</Container>
					</View>
				</View>
			</Root>
		);
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
	},
	preview: {
		flex: 3,
		backgroundColor: "#000000",
	},
	mform: {
        padding: 10,
		flex: 4,
		backgroundColor: '#17afbd',
	},
	content: {
        marginTop: 40,
        width: 280,
        height: 'auto',
    },
    title: {
        fontSize: 20,
        color: 'white',
    },
    inputText: {
        backgroundColor: '#99e5f3',
        color: 'white',
        marginBottom: 4,
        borderBottomColor: 'transparent',
        paddingLeft: 5,
        paddingRight: 5,
        width: 266,
        marginLeft: 7,
    },
    btn: {
        padding: 16,
    },
    btn_text: {
        color: '#e52427',
    }
});

export default MyClass;
