import React, { Component } from 'react';
import {
	Text, 
	View,
	StyleSheet,
	ListView,
	Image,
	TouchableHighlight,
	Button, 
} from 'react-native';
import { Spinner } from 'native-base';
// import Auth from '../components/Auth';

const API_URL = 'https://api.somosdar.mx/winners/search';

var InfoToken = {
	token      : 'XkY97z5bM',
	tokensecret: 'b779c79c-bee8-4ba0-ac64-162144bf3745',
	status: 1
}

const navigatorStyle = {
    statusBarColor: 'black',
    statusBarTextColorScheme: 'light',
    navigationBarColor: 'black',
    navBarBackgroundColor: '#e52427',
    navBarTextColor: 'white',
    navBarButtonColor: 'white',
    tabBarButtonColor: 'red',
    tabBarSelectedButtonColor: 'red',
    tabBarBackgroundColor: 'white',
    topBarElevationShadowEnabled: false,
    navBarHideOnScroll: true,
    tabBarHidden: true,
    drawUnderTabBar: true
};

class Foundations extends Component {
	constructor(props) {
	  	super(props);

	 //  	this.props.navigator.toggleNavBar({
  //           to: 'show', 
  //           animated: true,
  //           navigatorStyle
  //       });
	 //  	this.props.navigator.setDrawerEnabled({
  //           side: 'right', 
  //           enabled: true 
  //       });
  //       this.props.navigator.setButtons({
  // 			rightButtons: [
  //           	{
  //               	id: 'sideMenu'
  //           	}
  // 			], 
  // 			animated: true
		// });
	
	  	this.state = {
	  		dataSource: new ListView.DataSource({
	  			rowHasChanged: (row1, row2) => row1 !== row2 
	  		}),
	  		loaded: false,
	  	};
	}
	componentDidMount() {
		this.fetchData();
	}
	fetchData() {
		fetch(API_URL,InfoToken)
		.then((response) => response.json())
		.then((response) => {
			console.log(response)
			this.setState({
				dataSource: this.state.dataSource.cloneWithRows(response.items),
				loaded: true

			})
		})
	}
	renderLoadingView() {
		return (
			<View style={styles.container}>
				<Spinner color='red' />
			</View>
		);
	}
	renderItems(item) {
		console.log(item)
		return (
			<TouchableHighlight>
				<Image source={{uri: 'https://uploads.somosdar.mx/'+item.challenge.photo}} style={styles.backgroundImage}>
					<View style={styles.rightContainer}>
						<Image style={styles.avatar_user} source={{uri: 'https://uploads.somosdar.mx/'+item.user.photo}}/>
						<View style={{flex: 1}}>
							<Text style={styles.title}> {item.user.name}</Text>
							<Text style={styles.mail}> {item.user.mail}</Text>
							<Text style={styles.descript}>{item.challenge.name}</Text>
							<Text style={styles.descript}>{item.challenge.details}</Text>
						</View>
						<Image style={styles.avatar_submission} source={{uri: 'https://uploads.somosdar.mx/'+item.submission.photo}}/>
					</View>
				</Image>
			</TouchableHighlight>
		);
	}
    render() {
        if (!this.state.loaded) {
        	return this.renderLoadingView();
        }
        return (
        	<ListView
        		dataSource={this.state.dataSource}
        		renderRow={this.renderItems.bind(this)}
        		style={styles.ListView}
        	/>
    	);
    }
}

const styles = StyleSheet.create({
	container: {
		flexDirection: 'row',
		justifyContent: 'center',
		alignItems: 'center',
		backgroundColor: '#F5FCFF',
	},
	avatar_user: {
		flex: 1
	},
	avatar_submission: {
		flex: 1
	},
	title: {
		fontSize: 20,
		marginBottom: 2,
		textAlign: 'left',
		color: '#FFFFFF',
		backgroundColor: 'rgba(52,52,52,0)',
	},
	mail: {	
		fontSize: 18,
		color: '#FFFFFF',
		textAlign: 'justify',
	},
	descript: {
		fontSize: 15,
		marginTop: 5,
		textAlign: 'justify',
		color: '#FFFFFF',
		backgroundColor: 'rgba(52,52,52,0)'	
	},
	cause: {
		fontSize: 24,
		marginTop: 5,
		textAlign: 'left',
		color: '#FFFFFF',
		backgroundColor: 'rgba(52,52,52,0)'
	},
	backgroundImage: {
		justifyContent: 'center',
		alignItems: 'center',
		alignSelf: 'stretch',
		height: 220
	},
	rightContainer: {
		position: 'relative',
		backgroundColor: 'rgba(15, 108, 117, 0.55)',
		alignSelf: 'stretch',
		paddingTop: 30,
		height: 220
	},
	ListView: {
		marginBottom: 49
	},
	btn_donations: {
		position: 'absolute',
		bottom: 15,
		right: 15,
	},
	btn: {
		backgroundColor: 'rgba(0,0,0,0)',
		color: '#FFFFFF',
		borderColor: '#FFFFFF',
		borderWidth: 2,
	}


})

export default Foundations;