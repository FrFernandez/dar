import React, { Component } from 'react';
import {StyleSheet, View, Text, Dimensions} from 'react-native';
import {Spinner} from 'native-base';

class MyClass extends Component {
	constructor(props) {
		super(props);
	}

	render() {
		return (
			<View style={styles.bg}>
				<View style={styles.container}>
					<View>
						<Text style={styles.title}>{this.props.title}</Text>
						<Spinner color='blue' />
					</View>
				</View>
			</View>
		);
	}
}

const styles = StyleSheet.create({
	bg: {
		width: Dimensions.get('window').width,
		height: Dimensions.get('window').height,
		backgroundColor: 'rgba(0,0,0,0.5)'
	},
	container: {
		marginLeft: Dimensions.get('window').width * 0.15,
		marginTop: Dimensions.get('window').height * 0.35,
		width: Dimensions.get('window').width * 0.7,
		height: Dimensions.get('window').height * 0.3,
		backgroundColor: '#ffffff',
		borderRadius: 5,
		padding: 16,
	},
	title: {
		fontSize: 17,
		fontWeight: '700',
	},
	content: {
		marginTop: 8,
	},
});

export default MyClass;
