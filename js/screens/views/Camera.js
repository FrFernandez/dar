import React, { Component } from 'react';
import {
  AppRegistry,
  Dimensions,
  StyleSheet,
  Text,
  TouchableHighlight,
  TouchableOpacity,
  View
} from 'react-native';
import Camera from 'react-native-camera';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import {Navigation} from 'react-native-navigation';


class MyClass extends Component {
	constructor(props) {
		super(props);
		
		this.camera = null;

		this.props.navigator.toggleNavBar({
			to: 'hidden',
			animated: false
		});
		
		
		this.props.navigator.setDrawerEnabled({
			side: 'left',
			enabled: false
		});
		
		
		this.state = {
			challenge: props.challenge,
			camera: {
				aspect: Camera.constants.Aspect.fill,
				captureTarget: Camera.constants.CaptureTarget.cameraRoll,
				captureQuality: Camera.constants.CaptureQuality["480p"],
				type: Camera.constants.Type.back,
				orientation: Camera.constants.Orientation.auto,
				flashMode: Camera.constants.FlashMode.off,
			},
			isRecording: false
		};
	}

	takePicture = () => {
		/*let PayPal = require('react-native-paypal');
		PayPal.paymentRequest({
		  clientId: 'AbyfNDFV53djg6w4yYgiug_JaDfBSUiYI7o6NM9HE1CQ_qk9XxbUX0nwcPXXQHaNAWYtDfphQtWB3q4R',
		  environment: 'sandbox',
		  price: '42.00',
		  currency: 'USD',
		  description: 'PayPal Test'
		})
		.then((confirm, payment) => console.log('Paid'))
		.catch((error_code) => console.log('Failed to pay through PayPal'));
		*/	
		if (this.camera) {
			this.camera.capture()
			.then((data) => {
				data.challenge = this.state.challenge._id,
				this.props.navigator.push({
					screen: 'mx.somosdar.ImageView',
					title: 'Ver y Subir',
					passProps: data,
					animated: true,
					animationType: 'fade'
				})
			})
			.catch(err => console.error(err));
		}
	}

	startRecording = () => {
		if (this.camera) {
			this.camera.capture({mode: Camera.constants.CaptureMode.video})
				.then((data) => {
					data.challenge = this.state.challenge._id,
					this.props.navigator.push({
					  screen: 'mx.somosdar.VideoPlayer',
					  title: 'Ver y Subir',
					  passProps: data,
					  animated: true,
					  animationType: 'fade'
					})
				})
				.catch(err => console.error(err));
			this.setState({
				isRecording: true
			});
		}
	}

	stopRecording = () => {
		if (this.camera) {
			this.camera.stopCapture();
			this.setState({
				isRecording: false
			});
		}
	}

	switchType = () => {
		let newType;
		const { back, front } = Camera.constants.Type;

		if (this.state.camera.type === back) {
			newType = front;
		} else if (this.state.camera.type === front) {
			newType = back;
		}

		this.setState({
			camera: {
				...this.state.camera,
				type: newType,
			},
		});
	}

	switchFlash = () => {
		let newType;
		const { on, off } = Camera.constants.FlashMode;

		if (this.state.camera.flashMode === on) {
			newType = off;
		} else if (this.state.camera.flashMode === off) {
			newType = on;
		}

		this.setState({
			camera: {
				...this.state.camera,
				flashMode: newType,
			},
		});
	}

	get flashIcon() {
		let icon;
		const { on, off } = Camera.constants.FlashMode;

		if (this.state.camera.flashMode === on) {
			icon = "flash-off";
		} else if (this.state.camera.flashMode === off) {
			icon = "flash";
		}

		return icon;
	}

	get typeIcon() {
		let icon;
		const { back, front } = Camera.constants.Type;

		if (this.state.camera.type === back) {
			icon = "camera-front-variant";
		} else if (this.state.camera.type === front) {
			icon = "camera-rear-variant";
		}

		return icon;
	}


	render() {
		return (
		  <View style={styles.container}>
			<Camera
			  ref={(cam) => {
				this.camera = cam;
			  }}
			  style={styles.preview}
			  playSoundOnCapture={true}
			  aspect={this.state.camera.aspect}
			  captureTarget={this.state.camera.captureTarget}
			  captureQuality={this.state.camera.captureQuality}
			  type={this.state.camera.type}
			  //flashMode={this.state.camera.flashMode}
			  torchMode={this.state.camera.flashMode}
			  defaultTouchToFocus
			  mirrorImage={false}
			/>
			<View style={[styles.overlay, styles.topOverlay]}>
				
			</View>
			<View style={[styles.overlay, styles.bottomOverlay]}>
				
				
					<View style={styles.buttonsSpace}/>
					{
						!this.state.isRecording
						&&
						<TouchableOpacity
							style={styles.switchButton}
							onPress={this.switchType}
						>
							<Icon name={this.typeIcon} size={32} color={"#FFFFFF"}/>
						</TouchableOpacity>
						||
						null
					}
					<View style={styles.buttonsSpace}/>
					{
						!this.state.isRecording &&
						this.state.challenge.type == 1
						&&
						<TouchableOpacity
							style={styles.captureButton}
							onPress={this.takePicture}
						>
							<Icon name="camera" size={32} color={"#FFFFFF"}/>
						</TouchableOpacity>
						||
						null
					}
					{
						
						this.state.challenge.type == 1
						&&
						<View style={styles.buttonsSpace}/>
					}
					
					
					{
						!this.state.isRecording
						&&
						this.state.challenge.type == 2
						&&
						<TouchableOpacity
							style={styles.captureButton}
							onPress={this.startRecording}
						>
							<Icon name="video" size={32} color={"#FFFFFF"}/>
						</TouchableOpacity>
						||
						this.state.challenge.type == 2
						&&
						<TouchableOpacity
							style={styles.captureButton}
							onPress={this.stopRecording}
						>
							<Icon name="stop" size={32} color={"#FFFFFF"}/>
						</TouchableOpacity>
					}
					
					{
						this.state.challenge.type == 2
						&&
						<View style={styles.buttonsSpace}/>
					}
					
					<TouchableOpacity
						style={styles.switchButton}
						onPress={this.switchFlash}
					>
						<Icon name={this.flashIcon} size={32} color={"#FFFFFF"}/>
					</TouchableOpacity>
					
					
					
				</View>
		  </View>
		);
	}
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  preview: {
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  overlay: {
    position: 'absolute',
    padding: 16,
    right: 0,
    left: 0,
    alignItems: 'center',
  },
  topOverlay: {
    top: 0,
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  bottomOverlay: {
    bottom: 0,
    backgroundColor: 'rgba(0,0,0,0.4)',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  captureButton: {
    padding: 15,
    backgroundColor: '#E52427',
    borderRadius: 40,
  },
  switchButton: {
    padding: 15,
    backgroundColor: '#17AFBD',
    borderRadius: 40,
  },
  typeButton: {
    padding: 5,
  },
  flashButton: {
    padding: 5,
  },
  buttonsSpace: {
    width: 10,
  },
});

export default MyClass;
