import React, { Component } from 'react';
import {
	Text, 
	View,
	StyleSheet,
	ListView,
	Image,
	TouchableHighlight,
	Button, 
} from 'react-native';
import { Spinner } from 'native-base';
// import Auth from '../components/Auth';

const API_URL = 'https://api.somosdar.mx/foundations/search';

var InfoToken = {
	token      : 'XkY97z5bM',
	tokensecret: 'b779c79c-bee8-4ba0-ac64-162144bf3745',
	status: 1
}

const navigatorStyle = {
    statusBarColor: 'black',
    statusBarTextColorScheme: 'light',
    navigationBarColor: 'black',
    navBarBackgroundColor: '#e52427',
    navBarTextColor: 'white',
    navBarButtonColor: 'white',
    tabBarButtonColor: 'red',
    tabBarSelectedButtonColor: 'red',
    tabBarBackgroundColor: 'white',
    topBarElevationShadowEnabled: false,
    navBarHideOnScroll: true,
    tabBarHidden: true,
    drawUnderTabBar: true
};

class Foundations extends Component {
	constructor(props) {
	  	super(props);

	 //  	this.props.navigator.toggleNavBar({
  //           to: 'show', 
  //           animated: true,
  //           navigatorStyle
  //       });
	 //  	this.props.navigator.setDrawerEnabled({
  //           side: 'right', 
  //           enabled: true 
  //       });
  //       this.props.navigator.setButtons({
  // 			rightButtons: [
  //           	{
  //               	id: 'sideMenu'
  //           	}
  // 			], 
  // 			animated: true
		// });
	
	  	this.state = {
	  		dataSource: new ListView.DataSource({
	  			rowHasChanged: (row1, row2) => row1 !== row2 
	  		}),
	  		loaded: false,
	  	};
	}
	componentDidMount() {
		this.fetchData();
	}
	fetchData() {
		fetch(API_URL,InfoToken)
		.then((response) => response.json())
		.then((response) => {
			// console.log(response)
			this.setState({
				dataSource: this.state.dataSource.cloneWithRows(response.items),
				loaded: true

			})
		})
	}
	renderLoadingView() {
		return (
			<View style={styles.container}>
				<Spinner color='red' />
			</View>
		);
	}
	renderItems(item) {
		console.log(item)
		return (
			<TouchableHighlight>
				<Image source={{uri: 'https://uploads.somosdar.mx/'+item.photo}} style={styles.backgroundImage}>
					<View style={styles.rightContainer}>
						<Text numberOfLines ={1} style={styles.title}> {item.name}</Text>
						<Text numberOfLines ={5} style={styles.descript}>{item.details}</Text>
						<Text numberOfLines ={1} style={styles.cause}>{item.cause}</Text>
						<View style={styles.btn_donations}>
							<TouchableHighlight style={styles.btn}>
								<Text style={styles.btn_text}>Quiero Donar</Text>
							</TouchableHighlight>
						</View>
					</View>
				</Image>
			</TouchableHighlight>
		);
	}
    render() {
        if (!this.state.loaded) {
        	return this.renderLoadingView();
        }
        return (
        	<ListView
        		dataSource={this.state.dataSource}
        		renderRow={this.renderItems.bind(this)}
        		style={styles.ListView}
        	/>
    	);
    }
}

const styles = StyleSheet.create({
	container: {
		flexDirection: 'row',
		justifyContent: 'center',
		alignItems: 'center',
		backgroundColor: '#F5FCFF',
	},
	title: {
		fontSize: 27,
		width: 100,
		marginBottom: 8,
		textAlign: 'left',
		color: '#FFFFFF',
		backgroundColor: 'rgba(52,52,52,0)',
	},
	descript: {
		fontSize: 15,
		width: 100,
		marginTop: 5,
		textAlign: 'justify',
		color: '#FFFFFF',
		backgroundColor: 'rgba(52,52,52,0)'	
	},
	cause: {
		fontSize: 15,
		marginTop: 5,
		width: 100,
		textAlign: 'justify',
		color: '#FFFFFF',
		backgroundColor: 'rgba(52,52,52,0)',
		textDecorationLine: 'underline',
		textDecorationColor: '#FFFFFF',
	},
	backgroundImage: {
		justifyContent: 'center',
		alignItems: 'center',
		alignSelf: 'stretch',
		height: 180
	},
	rightContainer: {
		position: 'relative',
		backgroundColor: 'rgba(15, 108, 117, 0.55)',
		alignSelf: 'stretch',
		padding: 25,
		height: 180
	},
	ListView: {
		marginBottom: 49
	},
	btn_donations: {
		position: 'absolute',
		bottom: 15,
		right: 15,
	},
	btn: {
		backgroundColor: 'rgba(0,0,0,0)',
		padding: 10,
		borderRadius: 10,
		borderColor: '#FFFFFF',
		borderWidth: 2,
	},
	btn_text: {
		color: '#FFFFFF'
	}


})

export default Foundations;