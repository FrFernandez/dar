import {Navigation} from 'react-native-navigation';
import React, { Component, AsyncStorage } from 'react';
import { Root ,Toast } from 'native-base';

const API_URL = 'https://api.somosdar.mx/users';

var InfoToken = false;

class Auth extends Component {
	constructor(props) {
	  super(props);
	  this.state = {};
	  this._pushToken = this._pushToken.bind(this);
	  
	  
	}
	
	static async _getToken() {
		console.error("",AsyncStorage);
		try {
			InfoToken = {};
			InfoToken.token       = await AsyncStorage.getItem('token');
			InfoToken.tokensecret = await AsyncStorage.getItem('tokenSecret');

			console.error("infoToken", InfoToken);
			if (InfoToken.token && InfoToken.tokenSecret) {
				return InfoToken;
			}else {
				return false;
			}
		}catch(err) {
			console.error(err);
		}
		return InfoToken;
	}
	
	
	static async _pushToken(object){
		try {
			console.error(object);
			await AsyncStorage.setItem('token', object.token);
			await AsyncStorage.setItem('tokenSecret', object.tokenSecret);
		}catch(err) {
			console.error(err);
		}
	}
	
	static async _getData() {
		fetch(API_URL,object)
        .then((response) => response.json()) 
        .then((response) => {
            if (response.e===0) {
            	return response
            }else {
                return false
            }
        }) 
        .catch((err) => {
            console.error(err);
        })
	}
	return() {
		return null;
	}
}

export default Auth;