import React, { Component } from 'react';
import {Navigation} from 'react-native-navigation';

const API_URL = 'https://api.somosdar.mx/';

var params = {}

var Json = {};

class API_REST_FULL extends Component {
	constructor(props) {
		super(props);
		params = this.props.params;

		this.handlerHTTP(this.props.url);
		alert('Init API');
	}

	handlerHTTP(url) {
		switch (url) {
			case 'Auth':
				_Auth();
				break;
			
			default:
				console.log('Url Undefined');
				break;
		}
	}

	_Auth() {
		switch (this.props.methods) {
			case 'GETSingle':
				_get();
				break;
			case 'POSt':
				_post();
				break;
			case 'PUT':
				_put();
				break;
			default:
				console.log('Methods Undefined');
				break;
		}

		function _get() {
			return fetch(API_URL+'users', {
				method: 'GET',
  				headers: {
    				'Accept': 'application/json',
    				'Content-Type': 'application/json',
  				},
  				body: JSON.stringify(params)
			})
      			.then((response) => {
        			if (response.e===0) {
        				Json = response.item
        				this._return();
        			}
      			})
      			.catch((error) => {
        			console.error(error);
      			});
		}

		function _post() {
			return fetch(API_URL+'users', {
				method: 'POST',
  				headers: {
    				'Accept': 'application/json',
    				'Content-Type': 'application/json',
  				},
  				body: JSON.stringify(params)
			})
      			.then((response) => {
        			if (response.e===0) {
        				Json = response.item
        				this._return();
        			}
      			})
      			.catch((error) => {
        			console.error(error);
      			});
		}

		function _put() {
			return fetch(API_URL+'users', {
				method: 'PUT',
  				headers: {
    				'Accept': 'application/json',
    				'Content-Type': 'application/json',
  				},
  				body: JSON.stringify(params)
			})
      			.then((response) => {
        			if (response.e===0) {
        				Json = response.item
        				this._return();
        			}
      			})
      			.catch((error) => {
        			console.error(error);
      			});
		}
	}

	_return() {
		params = {};
		return Json;
	}

}

export default API_REST_FULL;